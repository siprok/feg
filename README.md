<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="resources/images/feg_icon.png" alt="Logo" width="60" height="60">
  </a>

<h3 align="center">FEG</h3>

  <p align="center">
    GUI and core to find function extreme and plot graphics 
    <br />
  </p>
</div>

## Installation

1. Clone the repo
   ```sh
   git clone git@gitlab.com:siprok/feg.git
   ```
2. Move to project folder
   ```sh
   cd feg
   ```
3. Create virtual environment
   ```sh
   python3 -m venv env
   ```
4. Activate the virtual environment
   Ubuntu version
   ```sh
   source env/bin/activate
   ```
   Windows version
   ```sh
   env/Scripts/activate.bat
   ```
5. Install the requirenments
   ```sh
   pip3 install -r req.txt
   ```


## Launch
1. Move to the project folder
2. Activate the virtual environment
   Ubuntu version
   ```sh
   source env/bin/activate
   ```
   Windows version
   ```sh
   env/Scripts/activate.bat
   ```
3. Launch app.py file
   ```sh
   python3 app.py
   ```