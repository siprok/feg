from dataclasses import InitVar, dataclass, field
from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
    QComboBox,
    QLabel,
    QLineEdit,
    QPushButton,
    QTableView,
    QFormLayout,
    QHBoxLayout,
    QVBoxLayout,
    QWidget
)


@dataclass(eq=False)
class DataView(QWidget):
    """Структура для хранения виджетов, размещаемых на странице выбор данных
    
    Parameters
    __________

    chooseFile : QPushButton
        Кнопка выброа файла
    separatorLabel : QLabel
        Заголовок для поля ввода раздедителя
    inputSeparator : QLineEdit
        Строка ввода раздедителя csv файла
    valueColumnLabel : QLabel
        Заголовок для выбора столбца значений
    chooseValueColumn : QComboBox
        Выпадающий список выбора столбца значений
    argsColumnLabel : QLabel
        Заголовок для выбора столбца аргумента функции
    chooseArgsColumn : QComboBox
        Выпадающий список выбора столбца аргумента функции
    timeColumnLabel : QLabel
        Заголовок для выбора столбца значений
    chooseTimeColumn : QComboBox
        Выпадающий список выбора столбца времени
    tableView : QTableView
        Виджет отображение таблицы данных
    """
    chooseFile: QPushButton  = field(default_factory=QPushButton)
    separatorLabel: QLabel = field(default_factory=QLabel)
    inputSeparator: QLineEdit = field(default_factory=QLineEdit)
    valueColumnLabel: QLabel = field(default_factory=QLabel)
    chooseValueColumn: QComboBox = field(default_factory=QComboBox)
    argsColumnLabel: QLabel = field(default_factory=QLabel)
    chooseArgsColumn: QComboBox = field(default_factory=QComboBox)
    timeColumnLabel: QLabel = field(default_factory=QLabel)
    chooseTimeColumn: QComboBox = field(default_factory=QComboBox)
    tableView: QTableView = field(default_factory=QTableView)
    parent: InitVar[QWidget] = None

    def __post_init__(self, parent):
        QWidget.__init__(self, parent)
        self.__setTitles()
        self.__settingUpInputs()
        self.__setAdjust()
        self.__formLayout()
        
    def __setTitles(self):
        self.chooseFile.setText("Выбрать файл")
        self.separatorLabel.setText("Разделитель в csv")
        self.valueColumnLabel.setText("Столбец значений")
        self.argsColumnLabel.setText("Столбец аргумента")
        self.timeColumnLabel.setText("Столбец времени")
    
    def __settingUpInputs(self):
        self.inputSeparator.setFixedWidth(52)
        self.inputSeparator.setMaxLength(4)

    def __setAdjust(self):
        for widget in (self.chooseValueColumn,
                        self.chooseArgsColumn,
                        self.chooseTimeColumn):
            widget.setSizeAdjustPolicy(QComboBox.AdjustToContents)

    def __formLayout(self, ):
        pageLayout = QHBoxLayout(self)
        layout = QVBoxLayout()
        layout.addWidget(self.chooseFile)
        separatorLayout = QFormLayout()
        separatorLayout.addRow(self.separatorLabel, self.inputSeparator)
        layout.addLayout(separatorLayout)
        valueColumnLayout = QFormLayout()
        valueColumnLayout.addRow(self.valueColumnLabel, self.chooseValueColumn)
        layout.addLayout(valueColumnLayout)
        argsColumnLayout = QFormLayout()
        argsColumnLayout.addRow(self.argsColumnLabel, self.chooseArgsColumn)
        layout.addLayout(argsColumnLayout)
        layout.setAlignment(Qt.AlignTop)
        pageLayout.addLayout(layout)
        pageLayout.addWidget(self.tableView, 1)

    @property
    def instances(self):
        for field in self.__dataclass_fields__:
            yield getattr(self, field)
