from .calcPageDelegate import CalcPageDelegate
from .dataPageDelegate import DataPageDelegate
from .modelPageDelegate import ModelPageDelegate
# from .infoPageDelegate import InfoPageDelegate


__all__ = (
    CalcPageDelegate,
    DataPageDelegate,
    # InfoPageDelegate,
    ModelPageDelegate,
)