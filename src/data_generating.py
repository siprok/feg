import numpy as np
import pandas as pd


dateTime = pd.date_range(start="1900-01-01", end="2020-01-01", freq='Y')
arguments = np.linspace(0, 1, 120)
values = np.exp(2*arguments)*np.sin(3*arguments)
noise = np.random.normal(size=120)
pd.DataFrame({
    "datetime": dateTime,
    "tau": arguments,
    "values": values,
    "noisy values": values + noise 
}).to_csv("test_data/example_dataset_2.csv", index=False)