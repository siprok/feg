from pathlib import Path
from PySide6 import QtCore
from PySide6.QtCore import QObject
from PySide6.QtWidgets import QWidget
from src.views import InfoView 


class InfoPageDelegate(QObject):
    def __init__(self, view: InfoView, parent: QWidget):
        QObject.__init__(self)
        sourcePath = Path().cwd() / Path("resources/infoPage.pdf")
        try:
            with open(sourcePath, "rb") as f:
                pass
        except FileNotFoundError:
            return 
        url = QtCore.QUrl.fromLocalFile(sourcePath)
        view.viewArea.load(url)