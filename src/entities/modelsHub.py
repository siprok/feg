from __future__ import annotations
from pathlib import Path
from .states import ModelState
from PySide6.QtCore import (
    Signal,
    QObject
)


class ModelsHub(QObject):
    
    modelAppended = Signal(str)

    def __init__(self, models: dict):
        QObject.__init__(self)
        if models:
            self.__models = models            
        else:
            self.__models = dict()

    def append(self, model: ModelState):
        self.__models[model.title] = model        
        self.modelAppended.emit(model.title)
    
    def get(self, title: str) -> ModelState:
        return self.__models[title]

    def getAllTitles(self) -> tuple:
        return tuple(self.__models.keys())

    def isConsistTitle(self, title: str):
        return title in self.__models

    @property
    def size(self):
        return len(self.__models)

    @classmethod
    def fromDir(cls, dirPath: Path) -> ModelsHub:
        models = ModelsHub([])
        for filePath in dirPath.glob("*.pkl"):
            models.append(ModelState.load(filePath))
        return models
