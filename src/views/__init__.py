from .calcView import CalcView
from .dataView import DataView
# from .infoView import InfoView
from .modelView import ModelView

__all__ = (
    CalcView,
    DataView,
    # InfoView,
    ModelView,
)