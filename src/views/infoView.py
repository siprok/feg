from dataclasses import InitVar, dataclass, field
from PySide6.QtWidgets import QWidget, QGridLayout
from PySide6 import (
    QtWebEngineWidgets,
    QtWebEngineCore,
)


@dataclass(eq=False)
class InfoView(QWidget):
    viewArea: QtWebEngineWidgets.QWebEngineView = field(default_factory=QtWebEngineWidgets.QWebEngineView)
    parent: InitVar[QWidget] = None

    def __post_init__(self, parent):
        QWidget.__init__(self, parent)
        self.viewArea.settings().setAttribute(QtWebEngineCore.QWebEngineSettings.PluginsEnabled, True)
        layout = QGridLayout(self)
        layout.addWidget(self.viewArea)