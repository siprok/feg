from pathlib import Path
from src.main_window import MainWindow
from src.entities import ModelsHub
from src.entities.states import DataState
from PySide6.QtWidgets import QApplication


if __name__ == '__main__':
    models = ModelsHub.fromDir(Path.cwd() / "resources/models")
    app = QApplication([])
    window = MainWindow(models, DataState())
    window.show()
    app.exec()