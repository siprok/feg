import numpy as np
import pandas as pd
from copy import copy
from numbers import Real
from typing import Optional
from dataclasses import dataclass
from PySide6.QtCore import Signal, Slot, QObject


@dataclass
class DataState(QObject):
    """Класс для хранения информации о данных во время сессии приложения

    Parameters
    __________

    data : pd.DataFrame, optional
        Датафрейм с данными
    valueIndex : str, optional
        Название столбца с данными
    argsIndex: str, optional
        Название столбца с аргументами для функции модели
    timeIndex : str, optional
        Название столбца со временем
    dataPath : str, optional
        Путь до файла, из которого были извлечены данные
    separator : str, optional
        Символ-разделитель в csv файле
    """
    data: Optional[pd.DataFrame] = None
    _valueIndex: Optional[int] = None
    _argsIndex: Optional[int] = None
    _timeIndex: Optional[int] = None
    path: Optional[str] = None
    separator: Optional[str] = None
    __trainSize: Optional[int] = None

    columnsUpdated: Signal = Signal()
    dataUpdated: Signal = Signal()
    valueIndexUpdated: Signal = Signal(int)
    argsIndexUpdated: Signal = Signal(int)
    timeIndexUpdated: Signal = Signal(int)

    def __post_init__(self):
        QObject.__init__(self)

    def updateStateFromFile(self):
        if not (self.path is None or self.path.isspace()):
            oldColumns = self.data.columns.values if self.data is not None else None
            self.data = pd.read_csv(self.path, sep=self.separator, engine="python")
            if not np.array_equal(oldColumns, self.data.columns.values):
                if self.data.columns.values.size > 0:
                    self.valueIndex = 0
                    self.argsIndex = 0
                    self.timeIndex = 0
                    self.__trainSize = self.data.shape[0]
                else:
                    self.clearColumnsLabels()
                self.columnsUpdated.emit()
            self.dataUpdated.emit()

    def clearColumnsLabels(self):
        self.valueIndex = None
        self.argsIndex = None
        self.timeIndex = None
    
    def getArguments(self) -> np.ndarray:
        if self.argsIndex is None:
            return np.ndarray([0])
        else:
            return self.__convertToFloat(self.data.iloc[:, self.argsIndex]).values

    def getValues(self) -> np.ndarray:
        if self.valueIndex is None:
            return np.ndarray([0])
        else:
            return self.__convertToFloat(self.data.iloc[:, self.valueIndex]).values

    def getTrainArguments(self) -> np.ndarray:
        if (self.argsIndex is None) or (self.__trainSize is None):
            return np.ndarray([])
        else:
            return self.__convertToFloat(self.data.iloc[:self.__trainSize, self.argsIndex]).values

    def getTrainValues(self) -> np.ndarray:
        if (self.valueIndex is None) or (self.__trainSize is None):
            return np.ndarray([])
        else:
            return self.__convertToFloat(self.data.iloc[:self.__trainSize, self.valueIndex]).values

    def getValidArguments(self) -> np.ndarray:
        if (self.argsIndex is None) or (self.__trainSize is None):
            return np.ndarray([])
        else:
            return self.__convertToFloat(self.data.iloc[self.__trainSize:, self.argsIndex]).values

    def getValidValues(self) -> np.ndarray:
        if (self.valueIndex is None) or (self.__trainSize is None):
            return np.ndarray([])
        else:
            return self.__convertToFloat(self.data.iloc[self.__trainSize:, self.valueIndex]).values

    def getDatetime(self) -> np.ndarray:
        if self.timeIndex is None:
            return np.ndarray([0])
        else:
            return self.data.iloc[:, self.timeIndex].values

    @property
    def valueIndex(self) -> Optional[int]:
        return self._valueIndex

    @Slot(int)
    @valueIndex.setter
    def valueIndex(self, value: Optional[int]):
        self._valueIndex = value
        self.valueIndexUpdated.emit(value)

    @property
    def argsIndex(self) -> Optional[int]:
        return self._argsIndex

    @Slot(int)
    @argsIndex.setter
    def argsIndex(self, value: Optional[int]):
        self._argsIndex = value
        self.argsIndexUpdated.emit(value)

    @property
    def timeIndex(self) -> Optional[int]:
        return self._timeIndex

    @Slot(int)
    @timeIndex.setter
    def timeIndex(self, value: Optional[int]):
        self._timeIndex = value
        self.timeIndexUpdated.emit(value)

    @Slot(int)
    def setTrainSize(self, value: int):
        self.__trainSize = value

    def __convertToFloat(self, array: pd.Series) -> pd.Series:
        if issubclass(array.values.dtype.type , Real):
            return array
        result = copy(array)
        try: 
            result = pd.Series(result).str.replace(',', '.').astype(float)
        except ValueError:
            pass
        finally:
            return result