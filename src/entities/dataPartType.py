from enum import Enum, auto


class DataPartType(Enum):
    TRAIN = auto()
    VALID = auto()
