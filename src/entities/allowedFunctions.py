import numpy as np

allowed_functions = {
        "exp": np.exp,
        "ln": np.log,
        "lg": np.log10, 
        "sin": np.sin,
        "cos": np.cos,
        "tg": np.tan,
    }