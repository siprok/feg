import pandas as pd
from src.views import DataView
from src.entities import (
    PandasModel,
)
from src.entities.states import DataState
from PySide6.QtCore import (
    Signal,
    Slot,
    QObject)
from PySide6.QtWidgets import (
    QFileDialog,
    QMessageBox,
    QWidget
)


class DataPageDelegate(QObject):
    """Сущность, отвечающая за обработку взаимодействия пользователя 
       с элементами графического интерфейса"""
    
    __separatorRejected = Signal(str)

    def __init__(self, dataState: DataState, view: DataView, parent: QWidget):
        QObject.__init__(self)
        self.dataState = dataState
        self.view = view
        self.parent = parent
        self.__createTableModel()
        self.__connectToViews()
    
    def __createTableModel(self):
        if self.dataState.data is None:
            self.tableModel = PandasModel(pd.DataFrame())
        else:
            self.tableModel = PandasModel(self.dataState.data)

    def __connectToViews(self):
        """Функция привязки обработкчиков событий к элементам грфического интерфейса"""
        # Привяжем обработчик нажатия кнопки выбор файла 
        self.view.chooseFile.clicked.connect(self.__handlerChooseSourceFile)
        # Привяжем обработчи ввода разделителя в csv файле
        self.view.inputSeparator.editingFinished.connect(self.__handlerInputSeparator)
        # Установим модель данных для таблицы отображения данных
        self.view.tableView.setModel(self.tableModel)
        # Привяжем обработку сигнала при невозможности считывания данных при введенном разделитиле
        self.__separatorRejected.connect(self.view.inputSeparator.setText)
        # Привяжем обработку обновления столбцов в данных
        self.dataState.columnsUpdated.connect(self.__updateColumnsChoosers)
        # Привяжем обновление данных к изменению отображения таблицы
        self.dataState.dataUpdated.connect(self.__updateTableModelData)
        # Привжем обработчики выбора столбцов
        for widget, handler in zip((self.view.chooseValueColumn,
                                    self.view.chooseArgsColumn,
                                    self.view.chooseTimeColumn),
                                    (self.__handlerValueIndexChoose,
                                    self.__handlerArgsIndexChoose,
                                    self.__handlerTimeIndexChoose)):
            widget.currentIndexChanged.connect(handler)   

    def __updateTableBySeparator(self, oldSeparator: str):
        try:
            self.dataState.updateStateFromFile()
        except pd.errors.ParserError as e:
            QMessageBox.warning(self, 
            "Ошибка чтения",
            str(e) + "\nНовые данные не были загружены",
            QMessageBox.Ok)
            self.dataState.separator = oldSeparator
            self.__separatorRejected.emit(oldSeparator)

    def __updateTableByPath(self):
        try:
            self.dataState.updateStateFromFile()
        except FileNotFoundError:
            QMessageBox.warning(self, 
            "Ошибка чтения",
            "Указанный файл не найден",
            QMessageBox.Ok)
        except pd.errors.EmptyDataError:
            QMessageBox.warning(self, 
            "Ошибка чтения",
            "Пустой файл" + "\nНовые данные не были загружены",
            QMessageBox.Ok)
        except pd.errors.ParserError as e:
            QMessageBox.warning(self, 
            "Ошибка чтения",
            str(e) + "\nНовые данные не были загружены",
            QMessageBox.Ok)
    
    @Slot()
    def __updateColumnsChoosers(self):
        """Функция обновления элементов выбора столбцов аргумента, времени и значений(статистики)"""
        newColumns = self.dataState.data.columns.values
        for widget in (self.view.chooseValueColumn,
                            self.view.chooseArgsColumn,
                            self.view.chooseTimeColumn):
            widget.clear()
            widget.addItems(newColumns)

    @Slot()
    def __updateTableModelData(self):
        self.tableModel.setDataFrame(self.dataState.data)

    @Slot()
    def __handlerChooseSourceFile(self):
        """Функция-обработчик нажатия на кнопку выбрать файл"""
        newPath = QFileDialog.getOpenFileName(self.parent, "Выбрать источником данных", "/home", "(*.csv)")[0]        
        if newPath == self.dataState.path or not newPath:
            return
        self.dataState.path = newPath
        self.__updateTableByPath()

    @Slot()
    def __handlerInputSeparator(self):
        """Функция обработчик окончания ввода разделителя для csv"""
        newSeparator = self.view.inputSeparator.text()
        oldSeparator = self.dataState.separator
        if newSeparator == oldSeparator:
            return
        if not newSeparator:
            self.dataState.separator = None
        else:
            self.dataState.separator = newSeparator
        self.__updateTableBySeparator(oldSeparator)

    @Slot(int)
    def __handlerValueIndexChoose(self, index: int):
        self.dataState.valueIndex = index

    @Slot(int)
    def __handlerArgsIndexChoose(self, index: int):
        self.dataState.argsIndex = index

    @Slot(int)
    def __handlerTimeIndexChoose(self, index: int):
        self.dataState.timeIndex = index