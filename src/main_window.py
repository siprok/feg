from PySide6.QtWidgets import (
    QMainWindow,
    QTabWidget,
    QWidget
)
from PySide6.QtCore import QObject
from PySide6.QtGui import QIcon
from typing import Generator, Tuple
from src.entities import (
    ModelsHub,
) 
from src.entities.states import DataState
from src.views import (
    CalcView,
    DataView,
    # InfoView,
    ModelView,
)
from src.delegates import (
    CalcPageDelegate,
    DataPageDelegate,
    # InfoPageDelegate,
    ModelPageDelegate
)


class MainWindow(QMainWindow):
    __WINDOW_TITLE = "FEG"
    __ICON_PATH = "./resources/images/feg_icon.png"
    __INFO_LABEL = "Описание"
    __DATA_LABEL = "Данные"
    __MODEL_LABEL = "Создать модель"
    __CALC_LABEL = "Рассчитать параметры"

    def __init__(self,
                 models: ModelsHub,
                 dataState: DataState):
        QMainWindow.__init__(self)
        self.dataState = dataState
        self.models = models
        self.tabs = []
        self.setWindowTitle(self.__WINDOW_TITLE)
        self.setWindowIcon(QIcon(self.__ICON_PATH))
        self.setGeometry(self.screen().availableGeometry())
        self.setTabs()

    def setTabs(self):
        """Установка вкладок переключения по окнам"""
        self.tabsWidget = QTabWidget(self)
        for label, widget in self.createTabWidgets():
            self.tabsWidget.addTab(widget, label)
        self.setCentralWidget(self.tabsWidget)
        
    def createTabWidgets(self) -> Generator[Tuple[str, QWidget, QObject], None, None]:
        """Фунцкия создания элементов страниц для переключения по окнам"""
        # Вкладка информации
        # self.tabs.append(
        #     {
        #         "label": self.__INFO_LABEL,
        #         "view": InfoView(parent=self.tabsWidget),
        #     }
        # )
        # self.tabs[-1]["delegate"] =  InfoPageDelegate(
        #     parent=self.tabsWidget,
        #     view=self.tabs[-1]["view"]
        # )
        # Вкладка данных
        self.tabs.append(
            {
                "label": self.__DATA_LABEL,
                "view": DataView(parent=self.tabsWidget),
            }
        )
        self.tabs[-1]["delegate"] =  DataPageDelegate(
            parent=self.tabsWidget, 
            dataState=self.dataState,
            view=self.tabs[-1]["view"]
        )
        # Вкладка моделей
        self.tabs.append(
            {
                "label": self.__MODEL_LABEL,
                "view": ModelView(parent=self.tabsWidget),
            }
        )
        self.tabs[-1]["delegate"] = ModelPageDelegate(
            parent=self.tabsWidget, 
            dataState=self.dataState,
            view=self.tabs[-1]["view"],
            models=self.models
        )
        # Вкладка вычислений
        self.tabs.append(
            {
                "label": self.__CALC_LABEL,
                "view": CalcView(parent=self.tabsWidget),
            }
        )
        self.tabs[-1]["delegate"] = CalcPageDelegate(
            parent=self.tabsWidget,
            dataState=self.dataState,
            view=self.tabs[-1]["view"], 
            models=self.models
        )
        #
        return ((d["label"], d["view"]) for d in self.tabs)