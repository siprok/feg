from dataclasses import InitVar, dataclass, field
import PySide6  # чтобы pyqtgraph использовала обертку над pyside6
import pyqtgraph as pg
from PySide6.QtCore import Qt
from PySide6.QtCore import Slot
from PySide6.QtGui import QRegularExpressionValidator
from PySide6.QtWidgets import (
    QFormLayout,
    QGroupBox,
    QHBoxLayout,
    QLabel,
    QLineEdit,    
    QPushButton,
    QRadioButton,
    QScrollArea,
    QSizePolicy,
    QVBoxLayout,
    QWidget
)


@dataclass(eq=False)
class ModelView(QWidget):
    """Структура для хранения виджетов, размещаемых на странице управления моделями
    
    Parameters
    __________

    funcTitleLabel : QLabel
        Заголовок для поля ввода названия модели
    funcTitleInput : QLineEdit
        Строка ввода названия модели
    funcArgsLabel : QLabel
        Заголовок для поля ввода аргументов функции
    funcArgsInput : QLineEdit
        Строка ввода аргументов функции
    funcExpressionLabel : QLabel
        Заголовок для поля ввода выражения функции модели
    funcExpressionInput : QLineEdit
        Строка ввода выражения функции модели
    argTypesLabel : QLabel
        Заголовок для списка элементов выбора типа переменной (параметр/гиперпараметр)    
    argTypesWidget : QWidget
        Виджет элементов выбора типа переменной
    argTypesScroll : QScrollBar
        Область прокрутки для списка элементов выбора типа переменной (параметр/гиперпараметр)
    previewArgsLabel: QLabel
    previewArgsWidget: QWidget
    previewArgsScroll: QScrollArea
    previewTypesLabel : QLabel
        Заголовок для списка элементов ввода значений переменных для просмотра графика функции
    previewTypesWidget : QWidget
        Виджет элементов ввода значений переменных для просмотра графика функции
    previewArgsScroll : QScrollArea
        Область прокрутки для списка элементов ввода значений переменных для просмотра графика функции
    domainLabel : QLabel
        Заголовок ввода границ значение абсциссы для предпросмотра
    domainLeft : QLineEdit
        Поле ввода минимальной границы значения абсциссы для предпросмотра
    domainRight : QLineEdit
        Поле ввода максимаьной границы значения абсциссы для предпросмотра
    sizeLabel: QLabel
    sizeInput: QLineEdit
    saveButton : QPushButton
        Кнопка сохранения модели
    preview : PlotWidget
        График введенной функции
    
    plotPreviewButton: QPushButton
    """
    funcTitleLabel: QLabel = field(default_factory=QLabel)
    funcTitleInput: QLineEdit = field(default_factory=QLineEdit)
    funcArgsLabel: QLabel = field(default_factory=QLabel)
    funcArgsInput: QLineEdit = field(default_factory=QLineEdit)
    funcExpressionLabel: QLabel = field(default_factory=QLabel)
    funcExpressionInput: QLineEdit = field(default_factory=QLineEdit)
    argTypesLabel: QLabel = field(default_factory=QLabel)
    argTypesWidget: QWidget = field(default_factory=QWidget)
    argTypesScroll: QScrollArea = field(default_factory=QScrollArea)
    previewArgsLabel: QLabel = field(default_factory=QLabel)
    previewArgsWidget: QWidget = field(default_factory=QWidget)
    previewArgsScroll: QScrollArea = field(default_factory=QScrollArea)
    domainLabel: QLabel = field(default_factory=QLabel)
    domainLeft: QLineEdit = field(default_factory=QLineEdit)
    domainRight: QLineEdit = field(default_factory=QLineEdit)
    sizeLabel: QLabel = field(default_factory=QLabel)
    sizeInput: QLineEdit = field(default_factory=QLineEdit)
    saveButton: QPushButton = field(default_factory=QPushButton)
    preview: pg.PlotWidget = field(default_factory=pg.PlotWidget)
    plotPreviewButton: QPushButton = field(default_factory=QPushButton)
    parent: InitVar[QWidget] = None

    def __post_init__(self, parent):
        QWidget.__init__(self, parent)
        self.__configureElements()
        self.__placementElements()       

    def __configureElements(self):
        """Функция настройки элементов интерфейса, расположенных на странице"""
        self.setTitles()
        self.configureArgTypes()
        self.createAndSetInputValidtors()
        self.configurePreviewArgs()
        self.configurePlotArea()

    def __placementElements(self):
        """Функция размещения элементов страницы"""
        pageLayout = QHBoxLayout(self)        
        pageLayout.addLayout(self.__formModelLayout())
        pageLayout.addLayout(self.__formPreviewLayout())

    def __formModelLayout(self) -> QVBoxLayout:
        """Функция создания группы объектов настроек модели"""
        layout = QVBoxLayout()
        layout.addWidget(self.funcTitleLabel)
        layout.addWidget(self.funcTitleInput)
        layout.addWidget(self.funcArgsLabel)
        layout.addWidget(self.funcArgsInput)
        layout.addWidget(self.funcExpressionLabel)
        layout.addWidget(self.funcExpressionInput)
        layout.addWidget(self.argTypesLabel)
        layout.addWidget(self.argTypesScroll)
        layout.addWidget(self.saveButton)
        layout.setAlignment(Qt.AlignTop)
        return layout

    def __formPreviewLayout(self) -> QVBoxLayout:
        """Функция создания группы объектов настроек модели"""
        layout = QVBoxLayout()
        layout.addWidget(self.preview)
        layout.addWidget(self.previewArgsLabel)
        layout.addWidget(self.previewArgsScroll)
        domainLayout = QHBoxLayout()
        domainLayout.addWidget(self.domainLabel)
        domainLayout.addWidget(self.domainLeft)
        domainLayout.addWidget(self.domainRight)
        domainLayout.addWidget(self.sizeLabel)
        domainLayout.addWidget(self.sizeInput)
        layout.addLayout(domainLayout)
        layout.addWidget(self.plotPreviewButton)
        layout.setAlignment(Qt.AlignTop)
        return layout

    def addElementsToArgType(self, argLabel: str, parameters: set, hyperparameters: set):
        """Функция добавления элемента выбора типа переменной"""
        @Slot()
        def handlerParam(self):
            hyperparameters.discard(argLabel)
            parameters.add(argLabel)
        @Slot()
        def handlerHyperparam(self):
            parameters.discard(argLabel)
            hyperparameters.add(argLabel)
        row = QGroupBox()
        row_layout = QHBoxLayout()
        label = QLabel()
        label.setText(argLabel)
        row_layout.addWidget(label)
        paramButton = QRadioButton("Параметр", row)
        paramButton.toggled.connect(handlerParam)
        row_layout.addWidget(paramButton)
        hyperparamButton = QRadioButton("Гиперпараметр", row)
        hyperparamButton.toggled.connect(handlerHyperparam)
        row_layout.addWidget(hyperparamButton)
        row.setLayout(row_layout)
        self.argTypesWidget.layout().addWidget(row)

    def addElementsToPreviewArg(self, argLabel: str, argValues: dict):
        """Функция добавления элемента ввода значения для предпросмотра"""
        argValues[argLabel] = None
        inputField = QLineEdit()
        @Slot()
        def handler():
            inputState = inputField.text()
            if not inputState:
                argValues[argLabel] = None
            else:    
                argValues[argLabel] = float(inputState)
        inputField.editingFinished.connect(handler)
        validator = QRegularExpressionValidator("(\-?\d{1,10}(\.\d+)?(E\-?\d{1,2})?)?")
        inputField.setValidator(validator)
        self.previewArgsWidget.layout().addRow(argLabel, inputField)

    def clearAllFields(self):
        self.funcTitleInput.setText("")
        self.funcArgsInput.setText("")
        self.funcExpressionInput.setText("")
        self.domainLeft.setText("")
        self.domainRight.setText("")
        self.sizeInput.setText("")
        self.preview.clear()
        self.removeArgTypeElements(None)
        self.removePreviewArgElements(None)

    def configureArgTypes(self):
        """Функция настройки области отображения элементов выбора типа аргументов фунцкии"""
        self.argTypesScroll.verticalScrollBarPolicy()
        self.argTypesScroll.setWidgetResizable(True)
        self.argTypesScroll.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
        self.argTypesWidget.setLayout(QVBoxLayout())
        self.argTypesScroll.setWidget(self.argTypesWidget)

    def configurePlotArea(self):
        """Функция настройки области отображения графика"""
        self.preview.setBackground("w")
        self.preview.showGrid(x=True, y=True)

    def configurePreviewArgs(self):
        """Функция настройки области области установки значения переменных для превью"""
        self.previewArgsScroll.verticalScrollBarPolicy()
        self.previewArgsScroll.setWidgetResizable(True)
        self.previewArgsScroll.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
        self.previewArgsWidget.setLayout(QFormLayout())
        self.previewArgsScroll.setWidget(self.previewArgsWidget)

    def createAndSetInputValidtors(self):
        """Функция установки валидаторов для строк ввода"""
        # Определим валидаторы
        titleValidator = QRegularExpressionValidator("([a-zA-Z\d]+\s?)*")
        argumentsValidator = QRegularExpressionValidator("([a-z]+\s?)*")
        expressionVAlidator = QRegularExpressionValidator(r"([\w\(\)]+[\.\+/\%\-]?([^ \.\+/\%\-]\*?)?)*")
        domainValidator = QRegularExpressionValidator("(\-?\d{1,10}(\.\d+)?(E\-?\d{1,2})?)?")
        sizeValidator = QRegularExpressionValidator("\d{0,6}")
        # Установим валидаторы
        self.funcTitleInput.setValidator(titleValidator)
        self.funcArgsInput.setValidator(argumentsValidator)
        self.funcExpressionInput.setValidator(expressionVAlidator)
        self.domainLeft.setValidator(domainValidator)
        self.domainRight.setValidator(domainValidator)
        self.sizeInput.setValidator(sizeValidator)

    def removeArgTypeElements(self, targets: set):
        """Функция удаления элементов выбора типа переменной"""
        for i in reversed(range(self.argTypesWidget.layout().count())): 
            widget = self.argTypesWidget.layout().itemAt(i).widget()
            if (not widget.layout() is None) and ((targets is None) or (widget.layout().itemAt(0).widget().text() in targets)):
                widget.deleteLater()

    def removePreviewArgElements(self, targets: set):
        """Функция удаления ввода значения для предпросмотра"""
        for i in reversed(range(self.previewArgsWidget.layout().count())):  
            
            variableLabel = self.previewArgsWidget.layout().itemAt(i, QFormLayout.LabelRole)
            if variableLabel is None:
                continue
            label = variableLabel.widget().text()  
            if (targets is None) or (label in targets):
                self.previewArgsWidget.layout().removeRow(i)
    
    def setTitles(self):
        # Установим текст кнопки сохранения модели
        self.saveButton.setText("Сохранить модель")
        # Установим текст кнопки отрисовки предпросмотра функции
        self.plotPreviewButton.setText("Построить функцию")
        # Установим текст для заголовка ввода названия модели
        self.funcTitleLabel.setText("Введите уникальное название модели")
        # Установим текст для заголовка ввода аргументов модели
        self.funcArgsLabel.setText("Введите аргументы модели через пробел")
        # Установим текст для заголовка ввода выражения функции модели
        self.funcExpressionLabel.setText("Введите функцию (используйте X для переменной)")
        # Установим текст в заголовок области выбора типа переменных
        self.argTypesLabel.setText("Выбирите тип переменных")
        # Установим текст в заголовок области установки значения переменных для превью
        self.previewArgsLabel.setText("Установите значения переменных для предпросмотра функции ")
        # Установим текст для заголовка ввода названия модели
        self.domainLabel.setText("Введите диапазон для предпросмотра")
        # Установим текст поля ввода количества точек для предпросмотра
        self.sizeLabel.setText("Введите количетсво точек предпросмотра")
    