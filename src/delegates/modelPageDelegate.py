import warnings
import numpy as np
from pathlib import Path
from copy import deepcopy
from src.entities.states import DataState, ModelState
from src.entities import ModelsHub
from src.views import ModelView
import PySide6
import pyqtgraph as pg
from PySide6.QtCore import Signal, Slot, QObject
from PySide6.QtWidgets import (
    QMessageBox,
    QWidget
)


class ModelPageDelegate(QObject):
    """Сущность, отвечающая за обработку взаимодействия пользователя 
       с элементами графического интерфейса"""

    def __init__(self, dataState: DataState, models: ModelsHub, view: ModelView, parent: QWidget):
        QObject.__init__(self)
        self.parent = parent
        self.view =  view
        self.models = models
        self.tmpModel = ModelState()
        self.previewArgValues = dict()
        self.hasPloted = False
        self.domainLeft = None
        self.domainRight = None
        self.previewSize = None
        self.plotPen = pg.mkPen(pg.mkColor('r'), width=5)
        self.__connectToViews()

    def __connectToViews(self):
        """Функция привязки обработкчиков событий к элементам грфического интерфейса"""
        # Установим обработчик ввода названия модели
        self.view.funcTitleInput.textChanged.connect(self.tmpModel.setTitle)
        # Установим обработчик ввода аргументов модели
        self.view.funcArgsInput.editingFinished.connect(self.__handlerInputFuncArgs)
        # Установим обработчик ввода выражения функции модели
        self.view.funcExpressionInput.editingFinished.connect(self.__handlerFuncExpressionInput)
        # Установим обработчик ввода минимальной границы абсциссы для предпросмотра
        self.view.domainLeft.editingFinished.connect(self.__handlerDomainLeft)
        # Установим обработчик ввода минимальной границы абсциссы для предпросмотра
        self.view.domainRight.editingFinished.connect(self.__handlerDomainRight)
        # Установим обработчик ввода количества точек предпросмотра
        self.view.sizeInput.editingFinished.connect(self.__handlerPreviewSize)
        # Добавим обработчик нажатия кнопки предпросмора функции
        self.view.plotPreviewButton.clicked.connect(self.__handlerPlotPreview)
        # Добавим обработчик нажатия кнопки сохранения модели
        self.view.saveButton.clicked.connect(self.__handlerSaveModel)

    ### Функции проверки корректности введенных данных ###
    def __isCorrectArgsTypes(self):
        # if len(self.tmpModel.parameters) < 1:
        #     QMessageBox.warning(self.parent, 
        #                 "Ошибка ввода аргументов модели",
        #                 f"Модель должна иметь хотя бы один параметр",
        #                 QMessageBox.Ok)
        #     return False
        emptyArgs = (set(self.tmpModel.arguments)
                     - self.tmpModel.parameters
                     - self.tmpModel.hyperparameters
                     )
        if len(self.tmpModel.arguments) > 0 and len(emptyArgs) > 0:
            QMessageBox.warning(self.parent, 
                        "Ошибка ввода аргументов модели",
                        f"Установите типы параметов {emptyArgs}",
                        QMessageBox.Ok)
            return False
        return True

    def __isCorrectDomainBorders(self):
        for value in (self.domainLeft, self.domainRight):
            if value is None:            
                QMessageBox.warning(self.parent, 
                            "Ошибка ввода аргументов предпросмотра",
                            f"Установите значения границ",
                            QMessageBox.Ok)
                return False
        if not self.domainLeft < self.domainRight:
            QMessageBox.warning(self.parent, 
                            "Ошибка ввода аргументов предпросмотра",
                            f"Левая граница должна быть меньше правой",
                            QMessageBox.Ok)
            return False
        return True

    def __isCorrectExpression(self):
        if not self.tmpModel.createFuncFromExpression():
            QMessageBox.warning(self.parent, 
                            "Ошибка ввода функции",
                            "ВВедено некорректное выражение",
                            QMessageBox.Ok)
            return False
        return True

    def __isCorrectPlotted(self):
        if not self.hasPloted:
            QMessageBox.warning(self.parent, 
                            "Ошибка сохранения функции",
                            "Необходимо построить предпросмотр функции, чтобы убедиться в корректности выражения",
                            QMessageBox.Ok)
            return False
        return True

    def __isCorrectPreviewSize(self):
        if self.previewSize is None:
            QMessageBox.warning(self.parent, 
                            "Ошибка ввода аргументов предпросмотра",
                            f"Установите значение колчества точек",
                            QMessageBox.Ok)
            return False
        return True

    def __isCorrectPreviewArgs(self):
        emptyArgs = []
        for label, value in self.previewArgValues.items():
            if value is None:
                emptyArgs.append(label)
        if len(emptyArgs) > 0:
            QMessageBox.warning(self.parent, 
                        "Ошибка ввода аргументов предпросмотра",
                        f"Установите значения аргументов {emptyArgs}",
                        QMessageBox.Ok)
            return False
        return True

    def __isCorrectTitle(self):
        result = True
        if not self.tmpModel.title:
            result = False
            QMessageBox.warning(self.parent, 
                        "Ошибка ввода заголовка функции",
                        f"Заголовок не должен быть пустым",
                        QMessageBox.Ok)
        if self.models.isConsistTitle(self.tmpModel.title):
            result = False
            QMessageBox.warning(self.parent, 
                    "Ошибка ввода заголовка функции",
                    f"Модель с таким заголовком уже существует",
                    QMessageBox.Ok)
        return result

    ### Обработчики событий ###
    @Slot()
    def __handlerDomainLeft(self):
        inputState = self.view.domainLeft.text()
        if not inputState:
            self.domainLeft = None
        else:    
            self.domainLeft = float(inputState)

    @Slot()
    def __handlerFuncExpressionInput(self):
        self.tmpModel.expression = self.view.funcExpressionInput.text()
        
    @Slot()
    def __handlerDomainRight(self):
        inputState = self.view.domainRight.text()
        if not inputState:
            self.domainRight = None
        else:    
            self.domainRight = float(inputState)
    
    @Slot()
    def __handlerInputFuncArgs(self):
        self.tmpModel.parseAndSetArgumets(self.view.funcArgsInput.text())
        new_args = set(self.tmpModel.arguments) - set(self.previewArgValues)
        not_actual_args = set(self.previewArgValues) - set(self.tmpModel.arguments)
        self.view.removeArgTypeElements(not_actual_args)
        self.view.removePreviewArgElements(not_actual_args)
        for key in not_actual_args:
            del self.previewArgValues[key]
        for argument in sorted(new_args):
            self.view.addElementsToArgType(argument, self.tmpModel.parameters, self.tmpModel.hyperparameters)
        for argument in sorted(new_args):
            self.view.addElementsToPreviewArg(argument, self.previewArgValues)

    @Slot()
    def __handlerPlotPreview(self):
        for checkResult in (self.__isCorrectPreviewArgs,
                            self.__isCorrectDomainBorders,
                            self.__isCorrectExpression,
                            self.__isCorrectPreviewSize):
            if not checkResult():
                return
        X = np.linspace(self.domainLeft, self.domainRight, self.previewSize)
        with warnings.catch_warnings():
            np.seterr(all='raise')
            try:
                funcValues = self.tmpModel.calc(X=X, kwargs=self.previewArgValues)
                self.view.preview.clear()
                self.view.preview.clearFocus()
                self.view.preview.plot(X, funcValues, pen=self.plotPen)
                self.hasPloted = True
            except (Warning, FloatingPointError):
                QMessageBox.warning(self.parent, 
                            "Ошибка вычисления функции",
                            "Введенная функция не определена на указанном диапазоне аргумента с указанными параметрами",
                            QMessageBox.Ok)
            except NameError:
                QMessageBox.warning(self.parent, 
                            "Ошибка вычисления функции",
                            "В функции встречаются переменные не внесенные в список вргументов",
                            QMessageBox.Ok)
                return               
    
    @Slot()
    def __handlerPreviewSize(self):
        inputState = self.view.sizeInput.text()
        if not inputState:
            self.previewSize = None
        else:    
            self.previewSize = int(inputState)

    @Slot()
    def __handlerSaveModel(self):
        for checkResult in (self.__isCorrectTitle,
                            self.__isCorrectExpression,
                            self.__isCorrectArgsTypes,
                            self.__isCorrectPlotted):
            if not checkResult():
                return
        self.models.append(deepcopy(self.tmpModel))
        self.tmpModel.dump(Path.cwd() / "resources/models/")
        self.tmpModel.clearAllFields()
        QMessageBox.information(self.parent, 
                            "Сохранение модели",
                            f"Модель успешно сохранена",
                            QMessageBox.Ok)
        self.view.clearAllFields()
        self.hasPloted = False