class BlankTitleException(Exception):
    pass

class BlankExpressionException(Exception):
    pass
