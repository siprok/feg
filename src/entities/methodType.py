from enum import Enum


class MethodType(Enum):
    STOCH = "Стохастический"
    GRAD = "Градиентный"
