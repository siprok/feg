import os
import warnings
import numpy as np
import pandas as pd
from typing import Dict, Optional
from pathlib import Path
from numbers import Real
from scipy import stats
from scipy.optimize import minimize
from hyperopt import (
    fmin, 
    hp, 
    rand, 
    Trials
)
from src.entities import (
    DataPartType,
    MethodType,
    ModelsHub,
)
from src.entities.states import DataState
from src.views import CalcView
import PySide6
import pyqtgraph as pg
from PySide6.QtCore import (
    Slot,
    QObject
)
from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
    QFileDialog,
    QMessageBox,
    QWidget
)
import pdb


class CalcPageDelegate(QObject):
    """Сущность, отвечающая за обработку взаимодействия пользователя 
       с элементами графического интерфейса"""
    
    def __init__(
        self,
        dataState: DataState,
        models: ModelsHub,
        view: CalcView,
        parent: QWidget,
    ):
        QObject.__init__(self)
        self.parent=parent
        self.dataState = dataState
        self.models = models
        self.view = view
        self.predictionStart = None
        self.predictionStep = None
        self.predictionSize = None
        self.optimizedParams: Dict[MethodType, Optional[dict]] =\
            {
                MethodType.GRAD: None,
                MethodType.STOCH: None,
            }
        self.paramsValues = dict()
        self.hyperparamsValues = dict()
        self.isPlotsVisible: Dict[MethodType, bool] = {
            MethodType.GRAD: self.view.gradientPlotCheckbox.isChecked(),
            MethodType.STOCH: self.view.stochasticPlotCheckbox.isChecked(),
        }
        self.isCorrdorPlotsVisible = self.view.validPlotCheckbox.isChecked()
        self.isValidPlotsVisible = self.view.corridorPlotCheckbox.isChecked()
        self.isPredicted = False
        self.plotPen = pg.mkPen(pg.mkColor('b'), width=5)
        self.modelPen = {
            DataPartType.TRAIN: {
                MethodType.STOCH: pg.mkPen(pg.mkColor('g'), width=5),
                MethodType.GRAD: pg.mkPen(pg.mkColor('k'), width=5),
            },
            DataPartType.VALID: {
                MethodType.STOCH: pg.mkPen(pg.mkColor((235,149,21)), width=5),
                MethodType.GRAD: pg.mkPen(pg.mkColor((142,21,235)), width=5),
            }
        }
        self.predPen = {
            MethodType.STOCH: pg.mkPen(pg.mkColor('r'), width=5),
            MethodType.GRAD: pg.mkPen(pg.mkColor('m'), width=5),
        }
        self.methodBrush = {
            MethodType.STOCH: pg.mkBrush(pg.mkColor('r')),
            MethodType.GRAD: pg.mkBrush(pg.mkColor('m')),
        }
        self.methodBrush[MethodType.STOCH].setStyle(Qt.BrushStyle.BDiagPattern)
        self.methodBrush[MethodType.GRAD].setStyle(Qt.BrushStyle.FDiagPattern)
        self.__connectToViews()
        self.__setInitData()

    def __connectToViews(self):
        # Установим обработчики событий по нажатию кнопки вычисления
        self.view.calcButton.clicked.connect(self.__handlerCalcModel)
        # Установим обновление размера тренировочной выборки по изменению данных
        self.dataState.dataUpdated.connect(self.__updateTrainSizeRange)
        self.dataState.dataUpdated.connect(self.__handlerExperimentChanged)
        # Установим отрисовку графика статистики при обновлении данных
        self.dataState.dataUpdated.connect(self.__handlerExperimentChanged)
        # Установим перерисовку графика статистики при обновлении выбора столбцов данных
        self.dataState.argsIndexUpdated.connect(
            self.__handlerExperimentChanged
        )
        self.dataState.valueIndexUpdated.connect(
            self.__handlerExperimentChanged
        )
        self.dataState.timeIndexUpdated.connect(
            self.__handlerExperimentChanged
        )
        # Установим обработчик на изменение размера тренировочной выборки
        self.view.trainSetSizeInput.valueChanged.connect(
            self.dataState.setTrainSize
        )
        self.view.trainSetSizeInput.valueChanged.connect(
            self.__handlerExperimentChanged
        )
        self.view.trainSetSizeInput.valueChanged.connect(
            self.__handlerTrinSetSizeChanged
        )
        # Установим обновление списка заголовков введенных функций при добавлении функции
        self.models.modelAppended.connect(self.__handlerModelAppended)
        # Установим обновление данных по выбору отображения
        self.view.funcTitleChooser.currentIndexChanged.connect(
            self.__handlerChooseFunc
        )
        # Установим обработчик ввода координаты начала прогноза
        self.view.predStartInput.editingFinished.connect(
            self.__handlerPredictionStart
        )
        # Установим обработчик ввода шага аргумента прогноза
        self.view.predStepInput.editingFinished.connect(
            self.__handlerPredictionStep
        )
        # Установим обработчик ввода количества точек прогноза
        self.view.predSizeInput.editingFinished.connect(
            self.__handlerPredictionSize
        )
        # Установим обработчик на кнопку формирования прогноза
        self.view.predButton.clicked.connect(self.__handlerMakePrediction)
        # Установим обработчик на кнопку скачивания материалов
        self.view.downloadButton.clicked.connect(self.__handlerDownload)
        # Установим обработчики на выбор видимоти графиков
        self.view.stochasticPlotCheckbox.stateChanged.connect(
            self.__handlerStochasticPlotsVisibleChanged
        )
        self.view.gradientPlotCheckbox.stateChanged.connect(
            self.__handlerGradientPlotsVisibleChanged
        )
        self.view.validPlotCheckbox.stateChanged.connect(
            self.__handlerValidPlotsVisibleChanged
        )
        self.view.corridorPlotCheckbox.stateChanged.connect(
            self.__handlerCorridorPlotsVisibleChanged
        )

    def __setInitData(self):
        for title in sorted(self.models.getAllTitles()):
            self.view.funcTitleChooser.addItem(title)
            self.view.funcTitleChooser.setCurrentIndex(0)

    def __hasCorrectData(self):
        if self.dataState.data is None:
            QMessageBox.warning(self.parent, 
                        "Ошибка данных",
                        f"Загрузите данные",
                        QMessageBox.Ok)
            return False
        if self.dataState.data.shape[0] < 1:
            QMessageBox.warning(self.parent, 
                        "Ошибка данных",
                        f"Данные должны быть не пусты",
                        QMessageBox.Ok)
            return False
        return True

    def __hasCorrectFunc(self):
        if self.models.size < 1:
            QMessageBox.warning(self.parent, 
                        "Ошибка модели",
                        f"Необходимо определить функцию для оптимизации",
                        QMessageBox.Ok)
            return False
        return True

    def __hasCorrectHyperparams(self):
        emptyArgs = []
        for label, value in self.hyperparamsValues.items():
            if value is None:
                emptyArgs.append(label)
        if len(emptyArgs) > 0:
            QMessageBox.warning(self.parent, 
                        "Ошибка ввода гиперпараметров",
                        f"Установите значения параметров {emptyArgs}",
                        QMessageBox.Ok)
            return False
        return True

    def __hasCorrectParams(self):
        emptyArgs = []
        for label, borders in self.paramsValues.items():            
            for _, value in borders.items():
                if value is None:
                    emptyArgs.append(label)
                    break
            if (label not in emptyArgs) and (borders["min"] >= borders["max"]):
                QMessageBox.warning(self.parent, 
                        "Ошибка ввода параметров",
                        f"Неверный порядок границ в параметре {label}. \
                            Сначала введите минимаьное значений, \
                                затем максимальное.",
                        QMessageBox.Ok)
                return False    
        if len(emptyArgs) > 0:
            QMessageBox.warning(self.parent, 
                        "Ошибка ввода параметров",
                        f"Установите границы поиска параметров {emptyArgs}",
                        QMessageBox.Ok)
            return False
        return True

    def __hasCorrectPredArgs(self):
        if not self.predictionStart:
            QMessageBox.warning(self.parent, 
            "Ошибка прогноза",
            f"Введите координату начала участка прогноза",
            QMessageBox.Ok)
            return False
        if not self.predictionStep:
            QMessageBox.warning(self.parent, 
            "Ошибка прогноза",
            f"Введите шаг абсциссы участка прогноза",
            QMessageBox.Ok)
            return False
        if not self.predictionSize:
            QMessageBox.warning(self.parent, 
            "Ошибка прогноза",
            f"Введите ненулевое количество точек на участке прогноза",
            QMessageBox.Ok)
            return False
        return True

    def __hasOptimised(self):
        # Проверим, что были найдены параметры модели
        title = self.view.funcTitleChooser.currentText()
        model = self.models.get(title)
        if (
            len(model.parameters) > 0
            and self.optimizedParams[MethodType.STOCH] is not None
            and self.optimizedParams[MethodType.GRAD] is not None
        ):
            return True
        elif len(model.parameters) == 0:
            return True
        # TODO: проверить, что параметры соответствуют текущей выбранной, а не предыдущей модели
        else:
            QMessageBox.warning(self.parent, 
                "Ошибка проноза",
                "Сначала необходимо найти значение параметров модели",
                QMessageBox.Ok)
        return False

    def __getOptParams(self) -> str:
        title = self.view.funcTitleChooser.currentText()
        model = self.models.get(title)
        paramSpace = {
            key: hp.uniform(key, val["min"], val["max"])
            for key, val in self.paramsValues.items()
        }
        trials_obj = Trials()
        x = self.dataState.getTrainArguments()
        y = self.dataState.getTrainValues()
        with warnings.catch_warnings():
            np.seterr(all='raise')
            try:
                # Поиск начального приближения стохастическим алгоритмом
                bestResults = fmin(
                    fn=model.getLoss(X=x, y=y, hyperParams=self.hyperparamsValues),
                    space=paramSpace,
                    algo=rand.suggest,
                    max_evals=1000,
                    trials=trials_obj
                )
                # Уточнение искомых значений параметров градиентным поиском
                sortedKeys = sorted(bestResults.keys()) 
                x0 = np.array([bestResults[key] for key in sortedKeys])
                replacedFunc = model.getLossFuncAfterReplace(
                    X=x,
                    y=y, 
                    hyperparamsValues=self.hyperparamsValues
                )
                gradSolve = minimize(replacedFunc, x0, options={"disp": True})
                bestGradResults = {
                    sortedKeys[i]: value for i, value in enumerate(gradSolve.x)
                }
                gradLoss = gradSolve.fun
                print(gradSolve)
                return (
                    bestResults,
                    trials_obj.best_trial['result']['loss'],
                    bestGradResults,
                    gradLoss,
                )
            except (Warning, FloatingPointError):
                QMessageBox.warning(self.parent, 
                            "Ошибка параметров",
                            "Измените условия поиска или алгоритм оптимизации",
                            QMessageBox.Ok)
                return None

    def __getDataToDownload(self) -> Dict[str, pd.DataFrame]:
        model_title = self.view.funcTitleChooser.currentText()
        model = self.models.get(model_title)
        # Модель
        modelDF = pd.DataFrame(
            {
                "Формула": [model.expression]
            }
        )
        # Данные истории
        hystoryArguments = self.dataState.getArguments()
        hystoryValues = self.dataState.getValues()
        hystoryDF = pd.DataFrame(
            {
                "Входные данные": hystoryArguments,
                "Целевые значения": hystoryValues, 
            }
        )
        # Данные обучающей выборки
        trainArguments = self.dataState.getTrainArguments()
        trainValues = self.dataState.getTrainValues()
        trainDF = pd.DataFrame(
            {
                "Входные данные": trainArguments,
                "Целевые значения": trainValues, 
            }
        )
        # Валидационные данные
        validArguments = self.dataState.getValidArguments()
        validValues = self.dataState.getValidValues()
        validDF = pd.DataFrame(
            {
                "Входные данные": validArguments,
                "Целевые значения": validValues, 
            }
        )
        # Гиперпараметры модели
        modelHyperDF = pd.DataFrame(
            {key: [value] for key, value in (self.hyperparamsValues).items()}
        )
        # Найденные параметры модели
        params_labels = sorted(self.optimizedParams[MethodType.STOCH].keys())
        paramsDF = pd.DataFrame(
            index=["Stochastic", "Stochastic+SGD"],
            columns=params_labels,
            data=[
                [params_dict[label] for label in params_labels]
                for params_dict in (
                    self.optimizedParams[MethodType.STOCH],
                    self.optimizedParams[MethodType.GRAD],
                )
            ]
        )
        # Данные выборки для прогноза
        predArguments = np.linspace(
            start=self.predictionStart,
            stop=self.predictionStart + self.predictionStep * self.predictionSize, 
            num=self.predictionSize)
        # Результаты на параметрах найденны при стохастическом поиске
        stochDF = pd.concat([
            pd.DataFrame({
                "Аппроксимация обучающей выборке": model.calc(
                    X=trainArguments,
                    kwargs = self.optimizedParams[MethodType.STOCH] | self.hyperparamsValues,
                )
            }),
            pd.DataFrame({
                "Прогноз на валидационной выборке": model.calc(
                    X=validArguments,
                    kwargs = self.optimizedParams[MethodType.STOCH] | self.hyperparamsValues,
                )
            }),
            pd.DataFrame({
                "Сгенерированные входные данные": predArguments,
                "Прогноз на сгенерированных данных": model.calc(
                    X=predArguments,
                    kwargs = self.optimizedParams[MethodType.STOCH] | self.hyperparamsValues,
                )
            }),
        ], axis=1)
        # Результаты на параметрах найденны при уточнении граиентным поиском
        gradDF = pd.concat([
            pd.DataFrame({
                "Аппроксимация обучающей выборке": model.calc(
                    X=trainArguments,
                    kwargs = self.optimizedParams[MethodType.GRAD] | self.hyperparamsValues,
                )
            }),
            pd.DataFrame({
                "Прогноз на валидационной выборке": model.calc(
                    X=validArguments,
                    kwargs = self.optimizedParams[MethodType.GRAD] | self.hyperparamsValues,
                )
            }),
            pd.DataFrame({
                "Сгенерированные входные данные": predArguments,
                "Прогноз на сгенерированных данных": model.calc(
                    X=predArguments,
                    kwargs = self.optimizedParams[MethodType.GRAD] | self.hyperparamsValues,
                )
            }),
        ], axis=1)
        return {
            "Модель": modelDF,
            "Все доступные данные": hystoryDF,
            "Обучающая выборка": trainDF,
            "Валидационная выборка": validDF,
            "Установленные гиперпараметры": modelHyperDF,
            "Найденные параметры": paramsDF,
            "Стохастический поиск": stochDF,
            "Уточнение градиентным поиском": gradDF,
        }

    def __plotDatasetData(self):
        arguments = self.dataState.getArguments()
        values = self.dataState.getValues()
        if not (issubclass(arguments.dtype.type, Real)
           and issubclass(values.dtype.type, Real)):
            return
        self.view.plotArea.plot(
            arguments,
            values,
            name="Статистика",
            symbol='o',
            symbolPen='b',
            pen='b',
        )
    
    def __plotData(
            self,
            dataPartType: DataPartType,
            methodType: MethodType,
        ) -> None:
        if dataPartType == DataPartType.TRAIN:
            arguments = self.dataState.getTrainArguments()
            name = "Аппроксимация на обучающей выборке"
        elif dataPartType == DataPartType.VALID:
            arguments = self.dataState.getValidArguments()
            name = "Аппроксимация на валидационной выборке"
        else:
            raise ValueError(f"dataPartType has unexpected value {dataPartType}")
        if methodType == MethodType.GRAD:
            name += "  | градиентный поиск"
        elif methodType == MethodType.STOCH:
            name += "  | вероятностный поиск"
        else:
            raise ValueError(f"methodType has unexpected value {methodType}")
        if arguments.size < 1:
            return
        title = self.view.funcTitleChooser.currentText()
        values = (
            self.models
            .get(title)
            .calc(
                X=arguments,
                kwargs = (
                    self.hyperparamsValues 
                    if self.optimizedParams[methodType] is None
                    else self.optimizedParams[methodType]
                        | self.hyperparamsValues
                ),
            )
        )
        self.view.plotArea.plot(
            arguments,
            values,
            pen=self.modelPen[dataPartType][methodType],
            name=name,
        )

    def __plotPredData(
            self,
            methodType: MethodType,
        ):
        arguments = np.linspace(
            start=self.predictionStart,
            stop=self.predictionStart + self.predictionStep * self.predictionSize, 
            num=self.predictionSize)
        title = self.view.funcTitleChooser.currentText()
        values = (
            self.models
            .get(title)
            .calc(
                X=arguments,
                kwargs = (
                    self.hyperparamsValues 
                    if self.optimizedParams[methodType] is None
                    else self.optimizedParams[methodType]
                        | self.hyperparamsValues
                ),
            )
        )
        if methodType == MethodType.STOCH:
            predName = "Прогноз"
            corridorName = "Корридор 2 сигма ошибки прогноза модели \
                после вероностной оптимизации"
        elif methodType == MethodType.GRAD:
            predName = "Прогноз утоненный"
            corridorName = "Корридор 2 сигма ошибки прогноза модели \
                после градиентной оптимизации"
        else:
            raise ValueError(f"method_type has unexpected value {methodType}")
        self.view.plotArea.plot(
            arguments,
            values,
            pen=self.predPen[methodType],
            name=predName,
        )
        if self.isCorrdorPlotsVisible:
            self.__plotErrorCorridor(
                x=arguments,
                y=values,
                historical=self.dataState.getTrainValues(),
                prediction=(
                    self.models.get(title)
                    .calc(
                        X=self.dataState.getTrainArguments(),
                        kwargs = self.optimizedParams[methodType] | self.hyperparamsValues
                    )
                ),
                brush=self.methodBrush[methodType],
                name=corridorName,
            )

    def __resetPlotArea(self):
        self.view.plotArea.clear()
        self.view.plotArea.clearFocus()
        self.view.plotArea.addLegend()

    def __resetFoundParams(self):
        self.optimizedParams[MethodType.STOCH] = None
        self.optimizedParams[MethodType.GRAD] = None
        self.isPredicted = False

    def __resetParamsView(self):
        self.view.foundParamsOutput.setText("")
        self.view.bestLossOutput.setText("")
        self.view.validLossOutput.setText("")
        self.view.foundGradParamsOutput.setText("")
        self.view.bestGradLossOutput.setText("")
        self.view.validGradLossOutput.setText("")

    def __resetOptimizationResult(self):
        self.__resetFoundParams()
        self.__resetParamsView()
        self.__resetPlotArea()

    def __plotWhateverPossible(self):
        self.__resetPlotArea()
        self.__plotDatasetData()
        title = self.view.funcTitleChooser.currentText()
        model = self.models.get(title)
        for methodType in MethodType:
            if (
                len(model.parameters) == 0
                or (
                    len(model.parameters) > 0
                    and self.optimizedParams[methodType]
                )
            ):
                if self.isPlotsVisible[methodType]:
                    self.__plotData(
                        dataPartType=DataPartType.TRAIN,
                        methodType=methodType,
                    )
                    if self.isValidPlotsVisible:
                        self.__plotData(
                            dataPartType=DataPartType.VALID,
                            methodType=methodType,
                        )
                    if self.isPredicted:
                        self.__plotPredData(methodType)

    def __plotErrorCorridor(
        self,
        x: np.ndarray,
        y: np.ndarray,
        historical: np.ndarray,
        prediction: np.ndarray,
        brush,
        name:str,
        confidense: float = 0.05,
    ) -> None:
        hist_len = historical.size
        upperCurve = pg.PlotCurveItem()
        lowerCurve = pg.PlotCurveItem()
        sigma = np.std(historical - prediction)
        scale_mult = (
            np.sqrt((hist_len + 1) / hist_len)
            + 3 * np.power(hist_len + 2 * np.arange(1, y.size + 1) - 1, 2)
                / (hist_len * (hist_len ** 2 - 1))
        )
        t = stats.t.isf(confidense / 2, hist_len - 1)
        shift = sigma * t * scale_mult
        print(shift)
        upperCurve.setData(x=x, y=y + shift)
        lowerCurve.setData(x=x, y=y - shift)
        self.view.plotArea.addItem(
            pg.FillBetweenItem(upperCurve, lowerCurve, brush=brush),
            name=name,
        )

    ### Обработички событий ###
    @Slot()
    def __handlerCalcModel(self):
        self.view.calcButton.setEnabled(False)
        self.__resetOptimizationResult()
        for checkResult in (self.__hasCorrectData,
                            self.__hasCorrectFunc,
                            self.__hasCorrectHyperparams,
                            self.__hasCorrectParams):
            if not checkResult():
                self.view.calcButton.setEnabled(True)
                return
        title = self.view.funcTitleChooser.currentText()
        model = self.models.get(title)
        if len(model.parameters) > 0:
            try:
                (
                    self.optimizedParams[MethodType.STOCH],
                    bestLoss,
                    self.optimizedParams[MethodType.GRAD],
                    bestGradLoss,
                ) = self.__getOptParams()
            except TypeError:
                self.view.calcButton.setEnabled(True)
                return
        else:
            self.optimizedParams[MethodType.STOCH] = None
            self.optimizedParams[MethodType.GRAD] = None
            bestLoss = model.getLoss(
                X=self.dataState.getTrainArguments(),
                y=self.dataState.getTrainValues(),
                hyperParams=self.hyperparamsValues
            )(kwargs={})
            bestGradLoss = bestLoss
        if self.optimizedParams[MethodType.STOCH] is not None\
            and self.optimizedParams[MethodType.GRAD] is not None:
            foundParamsOutput = ""
            foundGradParamsOutput = ""
            for k, v in self.optimizedParams[MethodType.STOCH].items():
                foundParamsOutput += f"{k}= {v:.2f}; "
            for k, v in self.optimizedParams[MethodType.GRAD].items():
                foundGradParamsOutput += f"{k}= {v:.2f}; "
            self.view.foundParamsOutput.setText(foundParamsOutput)
            self.view.foundGradParamsOutput.setText(foundGradParamsOutput)
        validArguments = self.dataState.getValidArguments()
        validValues = self.dataState.getValidValues()
        stochLossF = (
                model.getLoss(
                    X=validArguments,
                    y=validValues,
                    hyperParams=(
                        self.hyperparamsValues
                            if self.optimizedParams[MethodType.GRAD] is None
                            else self.optimizedParams[MethodType.GRAD] \
                                | self.hyperparamsValues
                    ),
                )
            if validArguments.size > 0
            else lambda kwargs: None
        )
        gradLossF = (
            model.getLoss(
                X=validArguments,
                y=validValues,
                hyperParams=(
                    self.hyperparamsValues
                        if self.optimizedParams[MethodType.GRAD] is None
                        else self.optimizedParams[MethodType.GRAD] \
                            | self.hyperparamsValues
                ),
            )
            if validArguments.size > 0
            else lambda kwargs: None
        )
        validLoss: Optional[float] = stochLossF(kwargs={})
        validGradLoss: Optional[float] = gradLossF(kwargs={})
        self.view.bestLossOutput.setText(
            f"{bestLoss:.2f}%"
            if bestLoss is not None
            else ""
        )
        self.view.validLossOutput.setText(
            f"{validLoss:.2f}%"
            if validLoss is not None
            else ""
        )
        self.view.bestGradLossOutput.setText(
            f"{bestGradLoss:.2f}%"
            if bestGradLoss is not None
            else ""
        )
        self.view.validGradLossOutput.setText(
            f"{validGradLoss:.2f}%"
            if validGradLoss is not None
            else ""
        )
        self.__plotWhateverPossible()
        self.view.calcButton.setEnabled(True)

    @Slot()
    def __handlerChooseFunc(self):
        self.__handlerExperimentChanged()
        self.view.funcExpressionOutput.setText('')
        title = self.view.funcTitleChooser.currentText()
        model = self.models.get(title)
        self.hyperparamsValues = dict()
        self.paramsValues = dict()
        self.view.funcExpressionOutput.setText(model.expression)
        self.view.clearScrollAreas()
        for hyperparam in sorted(model.hyperparameters):
            self.view.addElementsHyperparametersValuesInput(
                hyperparam,
                self.hyperparamsValues
            )
        for param in sorted(model.parameters):
            self.view.addElementsParametersValuesInput(param, self.paramsValues)
    
    @Slot(str)
    def __handlerModelAppended(self, title: str):
        self.view.funcTitleChooser.addItem(title)
 
    @Slot()
    def __handlerPredictionStart(self):
        inputState = self.view.predStartInput.text()
        if not inputState:
            self.predictionStart = None
        else:    
            self.predictionStart = float(inputState)

    @Slot()
    def __handlerPredictionStep(self):
        inputState = self.view.predStepInput.text()
        if not inputState:
            self.predictionStep = None
        else:    
            self.predictionStep = float(inputState)

    @Slot()
    def __handlerPredictionSize(self):
        inputState = self.view.predSizeInput.text()
        if not inputState:
            self.predictionSize = None
        else:    
            self.predictionSize = int(inputState)

    @Slot()
    def __handlerMakePrediction(self):
        self.view.calcButton.setEnabled(False)
        if not self.__hasOptimised():
            self.view.calcButton.setEnabled(True)
            return
        # Проверим корректность введенных агументов
        if not self.__hasCorrectPredArgs():
            self.view.calcButton.setEnabled(True)
            return
        with warnings.catch_warnings():
            np.seterr(all='raise')
            try:
                self.isPredicted = True
                self.__plotWhateverPossible()
            except (Warning, FloatingPointError):
                QMessageBox.warning(self.parent, 
                            "Ошибка вычисления прогноза",
                            "Введенная функция не определена на указанном\
                                диапазоне аргумента с указанными параметрами",
                            QMessageBox.Ok)
                self.isPredicted = False
                self.view.calcButton.setEnabled(True)
                return
        self.view.calcButton.setEnabled(True)

    @Slot()
    def __handlerDownload(self):
        self.view.downloadButton.setEnabled(False)
        if not self.__hasOptimised():
            self.view.downloadButton.setEnabled(True)
            return
        # Проверим корректность введенных агументов
        if not self.__hasCorrectPredArgs():
            self.view.downloadButton.setEnabled(True)
            return
        # TODO: сделать корректную обработку пол учения пути сохранения файла
        path = (
            QFileDialog.getSaveFileUrl(
            self.parent,
            "Сохранить в", "", "(*.xlsx)")[0]
            .path()
        )      
        if path[0] in { '/', '\\'} and os.name == "nt":
            path = path[1:]
        if path == self.dataState.path or not path:
            return
        if not path.endswith(".xlsx"):
            path += ".xlsx"
        with pd.ExcelWriter(path, mode="w", engine="xlsxwriter") as xlsx:
            for sheet_label, data in self.__getDataToDownload().items():
                data.to_excel(
                    xlsx,
                    sheet_name=sheet_label,
                    index=not issubclass(data.index.dtype.type, Real),
                )
                xlsx.sheets[sheet_label].autofit()
        self.view.calcButton.setEnabled(True)
        self.view.downloadButton.setEnabled(True)

    @Slot()
    def __handlerStochasticPlotsVisibleChanged(self, checked):
        self.isPlotsVisible[MethodType.STOCH] = bool(checked)
        self.__plotWhateverPossible()

    @Slot()
    def __handlerGradientPlotsVisibleChanged(self, checked):
        self.isPlotsVisible[MethodType.GRAD] = bool(checked)
        self.__plotWhateverPossible()

    @Slot()
    def __handlerValidPlotsVisibleChanged(self, checked):
        self.isValidPlotsVisible = bool(checked)
        self.__plotWhateverPossible()
    
    @Slot()
    def __handlerCorridorPlotsVisibleChanged(self, checked):
        self.isCorrdorPlotsVisible = bool(checked)
        self.__plotWhateverPossible()

    @Slot()
    def __handlerExperimentChanged(self):
        self.__resetOptimizationResult()
        self.__plotDatasetData()

    @Slot()
    def __updateTrainSizeRange(self):
        self.view.configureTrainSizeInput(
            1 
                if self.dataState.data.shape[0] > 0
                else 0,
            self.dataState.data.shape[0]
                if self.dataState.data.shape[0] > 0
                else 0,
        )
    
    @Slot(int)
    def __handlerTrinSetSizeChanged(self, value: int):
        self.view.configureValidSizeOutput(
            str(self.dataState.data.shape[0] - value)
        )
