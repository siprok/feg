from __future__ import annotations
import re
import dill
import numpy as np
from copy import copy
from pathlib import Path
from typing import Optional
from collections.abc import Callable
from dataclasses import dataclass, field
from ..allowedFunctions import allowed_functions
from PySide6.QtCore import Slot


def array2str(arr: np.ndarray) -> str:
    return f"ndarray([" + ",".join(arr.astype(str)) + "])"

def varibalesReplace(expression: str, old2new: dict) -> str:
    newStr = copy(expression)
    for old, new in old2new.items():
        assert isinstance(old, str) and isinstance(new, str)
        symbolFlag = True
        changedFlag = True
        while symbolFlag and changedFlag:
            changedFlag = False
            for m in re.finditer(old, newStr):
                if not m:
                    symbolFlag = False
                    break
                if m.start() > 0 and newStr[m.start() - 1].isalpha():
                    continue
                start = m.start()
                if m.end() < len(expression) and newStr[m.end()].isalpha():
                    continue
                end = m.end()
                newStr = newStr[:start] + new + newStr[end:]
                changedFlag = True
                break
    return newStr


@dataclass()
class ModelState:
    """Класс для хранения информации о модели"""
    __title: Optional[str] = None
    parameters: set = field(default_factory=set)
    hyperparameters: set = field(default_factory=set)
    __arguments: tuple = field(default_factory=tuple)
    __expression: Optional[str] = field(default_factory=str)
    __func: Callable[[dict], np.ndarray] = lambda kwargs: np.array([1])

    def calc(self,  X: np.ndarray, kwargs: dict) -> np.ndarray:
        return self.__func({**kwargs, **{"X": X}})

    def createFuncFromExpression(self) -> bool:
        try:
            compiledFunc = compile(self.__expression, '<string>', 'eval')
        except (SyntaxError, ValueError):
            return False
        self.__func = lambda kwargs: eval(
            compiledFunc, allowed_functions, kwargs
        )
        return True

    def parseAndSetArgumets(
        self,
        inputString: str,
    ) -> None:
        self.__arguments = tuple(re.findall(r"\w+", inputString.strip()))

    def getLoss(
        self,
        X: np.ndarray,
        y: np.ndarray,
        hyperParams: dict,
        eps: float=10**(-5),
    ) -> Callable:
        shift = np.equal(y, 0.) * eps
        return (
            lambda kwargs: 
            (np.abs(self.__func({**kwargs, **hyperParams, **{"X": X}})- y)
            / (np.abs(y) + shift)).sum() * 100 / y.size 
        )

    def getLossFuncAfterReplace(
        self,
        X: np.ndarray,
        y: np.ndarray,
        hyperparamsValues: dict,
        eps: float=10**(-5),
    ) -> Callable:
        assert np.isin(hyperparamsValues.keys(), self.hyperparameters).all()
        try:
            compile(self.__expression, '<string>', 'eval')
        except (SyntaxError, ValueError):
            return None
        paramsMapping = {
            key: f"x[{i}]"
            for i, key in enumerate(sorted(self.parameters))
        }
        replaced_expression = varibalesReplace(
            self.__expression,
            paramsMapping
        )
        replaced_func = compile(replaced_expression, '<string>', 'eval')
        constantsMapping = {**{"X": X}, **hyperparamsValues}
        shift = np.equal(y, 0.) * eps
        return (
            lambda x: (np.abs(eval(
                    replaced_func,
                    allowed_functions,
                    {**constantsMapping, **{'x': x}}
                ) - y)
                / (np.abs(y) + shift)).sum() * 100 / y.size 
        )

    @property
    def arguments(self):
        return copy(self.__arguments)

    @Slot(str)
    def setTitle(self, value: str):        
        self.__title = value

    @property
    def title(self):
        return self.__title    

    @property
    def expression(self):
        return copy(self.__expression)

    @expression.setter
    def expression(self, value: str):
        self.__expression = value

    def clearAllFields(self):
        self.__title = None
        self.parameters = set()
        self.hyperparameters = set()
        self.__arguments = tuple()
        self.__expression = None
        self.__func = lambda **kwargs: 1

    def dump(self, dirPath: Path):
        dirPath.mkdir(parents=True, exist_ok=True)
        with open(dirPath / (self.__title + ".pkl"), "wb") as f:
            dill.dump(self, f)

    @classmethod
    def load(cls, path: Path):
        with open(path, "rb") as f:
            return dill.load(f)
    