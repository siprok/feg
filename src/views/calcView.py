from dataclasses import InitVar, dataclass, field
import PySide6  # чтобы pyqtgraph использовала обертку над pyside6
import pyqtgraph as pg
from PySide6.QtCore import Slot, Qt
from PySide6.QtGui import QRegularExpressionValidator
from PySide6.QtWidgets import (
    QComboBox,
    QCheckBox,
    QFormLayout,   
    QGroupBox,
    QLabel,
    QLineEdit,    
    QPushButton,
    QScrollArea,
    QSizePolicy,
    QSpinBox,
    QHBoxLayout,
    QVBoxLayout,
    QWidget
)


@dataclass(eq=False)
class CalcView(QWidget):
    """Структура для хранения виджетов, размещаемых на странице оптимизации фунции
    
    Parameters
    __________

    funcTitleLabel : QLabel
        Заголовок для поля выбора названия модели
    funcTitleChooser : QComboBox
        Элемент выбора названия модели
    funcExpressionLabel : QLabel
        Заголовок для поля отображения выражения функции модели
    funcExpressionOutput : QLineEdit
        Строка отображения выражения функции модели
    foundParamsLabel : QLabel
        Заголовок для поля отображения найденных значений параметров модели
    foundParamsOutput : QLineEdit
        Строка отображения найденных значений параметров модели
    hyperparamsLabel : QLabel
        Заголовок для списка элементов ввода значений гиперпараметров
    hyperparamsWidget : QWidget
        Виджет элементов ввода значений гиперпараметров
    hyperparamsScroll : QScrollBar
        Область прокрутки для списка элементов ввода значений гиперпараметров
    paramsLabel : QLabel
        Заголовок для списка элементов ввода границ поиска значений параметров модели
    paramsWidget : QWidget
        Виджет элементов ввода границ поиска значений параметров модели
    paramsScroll : QScrollBar
        Область прокрутки для списка элементов ввода границ поиска значений параметров модели
    calcButton : QPushButton
        Кнопка запуска процесса оптимизации
    trainSetSizeLabel : QLabel
        Заголовок поля ввода размера обучающей выборки
    trainSetSizeInput : QSpinBox
        Поле ввода размера обучающей выборки
    predictButton : QPushButton
        Кнопка расчета прогноза согласно модели
    methodPlotsChooseLabel : QLabel
        Заголовок области выбора отображаемых графиков
    plotArea : PlotWidget
        График статистики и полученной модели
    predictionArgsLabel : QLabel
        Заголовок для списка полей ввода настроек прогноза
    predictionArgsWidget : QWidget
        Виджет полей ввода настроек прогноза
    predictionArgsScroll : QScrollArea
        Область прокрутки для полей элементов ввода настроек прогноза

    """
    funcTitleLabel: QLabel = field(default_factory=QLabel)
    funcTitleChooser: QComboBox  = field(default_factory=QComboBox)
    funcExpressionLabel: QLabel = field(default_factory=QLabel)
    funcExpressionOutput: QLineEdit = field(default_factory=QLineEdit)
    foundParamsLabel: QLabel = field(default_factory=QLabel)
    foundParamsOutput: QLineEdit = field(default_factory=QLineEdit)
    bestLossLabel: QLabel = field(default_factory=QLabel)
    bestLossOutput: QLineEdit = field(default_factory=QLineEdit)
    validLossLabel: QLabel = field(default_factory=QLabel)
    validLossOutput: QLineEdit = field(default_factory=QLineEdit)
    foundGradParamsLabel: QLabel = field(default_factory=QLabel)
    foundGradParamsOutput: QLineEdit = field(default_factory=QLineEdit)
    bestGradLossLabel: QLabel = field(default_factory=QLabel)
    bestGradLossOutput: QLineEdit = field(default_factory=QLineEdit)
    validGradLossLabel: QLabel = field(default_factory=QLabel)
    validGradLossOutput: QLineEdit = field(default_factory=QLineEdit)
    hyperparamsLabel: QLabel = field(default_factory=QLabel)
    hyperparamsWidget: QWidget = field(default_factory=QWidget)
    hyperparamsScroll: QScrollArea = field(default_factory=QScrollArea)
    paramsLabel: QLabel = field(default_factory=QLabel)
    paramsWidget: QWidget = field(default_factory=QWidget)
    paramsScroll: QScrollArea = field(default_factory=QScrollArea)
    calcButton: QPushButton = field(default_factory=QPushButton)
    trainSetSizeLabel : QLabel = field(default_factory=QLabel)
    trainSetSizeInput : QSpinBox = field(default_factory=QSpinBox)
    validSetSizeLabel : QLabel = field(default_factory=QLabel)
    validSetSizeOutput : QLineEdit = field(default_factory=QLineEdit)
    plotArea: pg.PlotWidget = field(default_factory=pg.PlotWidget)
    plotsChooseLabel: QLabel = field(default_factory=QLabel)
    validPlotCheckbox: QCheckBox = field(default_factory=QCheckBox)
    corridorPlotCheckbox: QCheckBox = field(default_factory=QCheckBox)
    methodPlotsChooseLabel: QLabel = field(default_factory=QLabel)
    stochasticPlotCheckbox: QCheckBox = field(default_factory=QCheckBox)
    gradientPlotCheckbox: QCheckBox = field(default_factory=QCheckBox)
    predictionArgsLabel: QLabel = field(default_factory=QLabel)
    predictionArgsWidget: QWidget = field(default_factory=QWidget)
    predStartLabel: QLabel = field(default_factory=QLabel)
    predStartInput: QLineEdit = field(default_factory=QLineEdit)
    predStepLabel: QLabel = field(default_factory=QLabel)
    predStepInput: QLineEdit = field(default_factory=QLineEdit)
    predSizeLabel: QLabel = field(default_factory=QLabel)
    predSizeInput: QLineEdit = field(default_factory=QLineEdit)
    predButton: QPushButton = field(default_factory=QPushButton)
    downloadButton: QPushButton = field(default_factory=QPushButton)
    parent: InitVar[QWidget] = None

    def __post_init__(self, parent):
        QWidget.__init__(self, parent)
        self.__configureElements()
        self.__placementElements()

    def __configureElements(self):
        """Функция настройки элементов интерфейса, расположенных на странице"""
        self.setTitles()
        self.configureTrainSizeInput()
        self.configureValidSizeOutput()
        self.configureOutputFields()
        self.configureHyperparametersDomain()
        self.configureParametersDomain()
        self.configurePlotArea()
        self.configurePredictionArgs()
        self.createAndSetInputValidtors()

    def __placementElements(self):
        """Функция размещения элементов страницы"""
        pageLayout = QHBoxLayout(self)
        pageLayout.addLayout(self.__formModelLayout())
        pageLayout.addLayout(self.__formPreviewLayout())

    def __formModelLayout(self) -> QVBoxLayout:
        """Функция создания группы объектов настроек модели"""
        layout = QVBoxLayout()
        layout.addWidget(self.funcTitleLabel)
        layout.addWidget(self.funcTitleChooser)
        layout.addWidget(self.funcExpressionLabel)
        layout.addWidget(self.funcExpressionOutput)
        layout.addWidget(self.foundParamsLabel)
        layout.addWidget(self.foundParamsOutput)
        layout.addWidget(self.bestLossLabel)
        layout.addWidget(self.bestLossOutput)
        layout.addWidget(self.validLossLabel)
        layout.addWidget(self.validLossOutput)
        layout.addWidget(self.foundGradParamsLabel)
        layout.addWidget(self.foundGradParamsOutput)
        layout.addWidget(self.bestGradLossLabel)
        layout.addWidget(self.bestGradLossOutput)
        layout.addWidget(self.validGradLossLabel)
        layout.addWidget(self.validGradLossOutput)
        layout.addWidget(self.hyperparamsLabel)
        layout.addWidget(self.hyperparamsScroll)
        layout.addWidget(self.paramsLabel)
        layout.addWidget(self.paramsScroll)
        trainSetSizeLayout = QHBoxLayout()
        trainSetSizeLayout.addWidget(self.trainSetSizeLabel)
        trainSetSizeLayout.addWidget(self.trainSetSizeInput)
        validSetSizeLayout = QHBoxLayout()
        validSetSizeLayout.addWidget(self.validSetSizeLabel)
        validSetSizeLayout.addWidget(self.validSetSizeOutput)
        layout.addLayout(trainSetSizeLayout)
        layout.addLayout(validSetSizeLayout)
        layout.addWidget(self.calcButton)
        layout.setAlignment(Qt.AlignTop)
        return layout

    def __formPreviewLayout(self) -> QVBoxLayout:
        """Функция создания группы объектов настроек модели"""
        layout = QVBoxLayout()
        layout.addWidget(self.plotArea)
        searchMethodCheckboxesLayout = QVBoxLayout()
        searchMethodCheckboxesLayout.addWidget(self.methodPlotsChooseLabel)
        searchMethodCheckboxesLayout.addWidget(self.stochasticPlotCheckbox)
        searchMethodCheckboxesLayout.addWidget(self.gradientPlotCheckbox)
        searchMethodCheckboxesLayout.setAlignment(Qt.AlignRight)
        addPlotsCheckboxesLayout = QVBoxLayout()
        addPlotsCheckboxesLayout.addWidget(self.plotsChooseLabel)
        addPlotsCheckboxesLayout.addWidget(self.validPlotCheckbox)
        addPlotsCheckboxesLayout.addWidget(self.corridorPlotCheckbox)
        addPlotsCheckboxesLayout.setAlignment(Qt.AlignRight)
        plotsChoosenLayout = QHBoxLayout()
        plotsChoosenLayout.addLayout(addPlotsCheckboxesLayout)
        plotsChoosenLayout.addLayout(searchMethodCheckboxesLayout)
        layout.addLayout(plotsChoosenLayout)
        layout.addWidget(self.predictionArgsLabel) 
        predArgsLayout = QHBoxLayout()
        predArgsLayout.addWidget(self.predStartLabel)
        predArgsLayout.addWidget(self.predStartInput)
        predArgsLayout.addWidget(self.predStepLabel)
        predArgsLayout.addWidget(self.predStepInput)
        predArgsLayout.addWidget(self.predSizeLabel)
        predArgsLayout.addWidget(self.predSizeInput)
        layout.addLayout(predArgsLayout)
        buttonsLayout = QHBoxLayout()
        buttonsLayout.addWidget(self.predButton)
        buttonsLayout.addWidget(self.downloadButton)
        layout.addLayout(buttonsLayout)
        layout.setAlignment(Qt.AlignTop)
        return layout    

    def configureHyperparametersDomain(self):
        """Фунцкия настройки области считывания знчаения гиперпараметров"""
        self.hyperparamsScroll.setWidgetResizable(True)
        self.hyperparamsScroll.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
        self.hyperparamsWidget.setLayout(QFormLayout())
        self.hyperparamsScroll.setWidget(self.hyperparamsWidget)

    def configureOutputFields(self):
        self.funcExpressionOutput.setReadOnly(True)
        self.foundParamsOutput.setReadOnly(True)
        self.foundGradParamsOutput.setReadOnly(True)
        self.bestLossOutput.setReadOnly(True)
        self.validLossOutput.setReadOnly(True)
        self.bestGradLossOutput.setReadOnly(True)
        self.validGradLossOutput.setReadOnly(True)
        self.validSetSizeOutput.setReadOnly(True)

    def configureParametersDomain(self):
        """Фунцкия настройки области считывания границ поиска значений параметров"""
        self.paramsScroll.setWidgetResizable(True)
        self.paramsScroll.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
        self.paramsWidget.setLayout(QVBoxLayout())
        self.paramsScroll.setWidget(self.paramsWidget)

    def configurePlotArea(self):
        """Функция настройки области отображения графика"""
        self.plotArea.setBackground("w")
        self.plotArea.showGrid(x=True, y=True)

    def configurePredictionArgs(self):
        self.predictionArgsWidget.setLayout(QFormLayout())

    def configureTrainSizeInput(self, low: int = 0, high: int = 0):
        self.trainSetSizeInput.setRange(low, high)

    def configureValidSizeOutput(self, value: str = "0"):
        self.validSetSizeOutput.setText(value)

    def setTitles(self):
        """Функция установки значения заголовков полей"""
        self.funcTitleLabel.setText("Выбирете модель")
        self.funcExpressionLabel.setText("Функция модели")
        self.foundParamsLabel.setText("Рассчитанные значения параметров модели случайным поиском")
        self.foundGradParamsLabel.setText("Рассчитанные значения параметров модели, уточненные SGD")
        self.bestLossLabel.setText("Средняя ошибка аппроксимации на обучающей выборке при найденных значениях параметров")
        self.validLossLabel.setText("Средняя ошибка аппроксимации на валидационной выборке")
        self.bestGradLossLabel.setText("Средняя ошибка аппроксимации на обучающей выборке при найденных значениях параметров, уточненных SGD")
        self.validGradLossLabel.setText("Средняя ошибка аппроксимации на валидационной выборке после SGD")
        self.hyperparamsLabel.setText("Введите значения гиперпараметров")
        self.paramsLabel.setText("Введите диапазоны для поиска параметров")
        self.trainSetSizeLabel.setText("Размер обучающей выборки")
        self.validSetSizeLabel.setText("Размер валидационной выборки")
        self.calcButton.setText("Рассчитать на известной выборке")
        self.plotsChooseLabel.setText("Отображамые графики")
        self.validPlotCheckbox.setText("Рассчет на валидационной выборке")
        self.corridorPlotCheckbox.setText("Коридор ошибки тестового прогноза")
        self.methodPlotsChooseLabel.setText("Графики по алгоритму поиска параметров")
        self.stochasticPlotCheckbox.setText("Вероятностный поиск методом")
        self.gradientPlotCheckbox.setText("Градиентный спуск")
        self.predictionArgsLabel.setText("Установите параметры аргумента для получения прогноза")   
        self.predStartLabel.setText("Координата начала")
        self.predStepLabel.setText("Шаг")
        self.predSizeLabel.setText("Количество точек")
        self.predButton.setText("Рассчитать прогноз")
        self.downloadButton.setText("Скачать расчеты")

    def addElementsHyperparametersValuesInput(self, argLabel: str, argValues: dict):
        """Функция добавления элемента ввода значений гиперпараметров модели"""
        argValues[argLabel] = None
        inputField = QLineEdit()
        @Slot()
        def handler():
            inputState = inputField.text()
            if not inputState:
                argValues[argLabel] = None
            else:    
                argValues[argLabel] = float(inputState)
        inputField.editingFinished.connect(handler)
        validator = QRegularExpressionValidator("(\-?\d{1,10}(\.\d+)?(E\-?\d{1,2})?)?")
        inputField.setValidator(validator)
        self.hyperparamsWidget.layout().addRow(argLabel, inputField)

    def addElementsParametersValuesInput(self, argLabel: str, argValues: dict):
        """Функция добавления элемента ввода диапазон значений параметров модели"""
        argValues[argLabel] = {"min": None, "max": None}
        row = QGroupBox()
        localLayout = QHBoxLayout()
        localLayout.addWidget(QLabel(argLabel))
    
        minInputField = QLineEdit()
        @Slot()
        def handlerMin():
            inputState = minInputField.text()
            if not inputState:
                argValues[argLabel]["min"] = None
            else:    
                argValues[argLabel]["min"] = float(inputState)
        minInputField.editingFinished.connect(handlerMin)
        validator = QRegularExpressionValidator("(\-?\d{1,10}(\.\d+)?(E\-?\d{1,2})?)?")
        minInputField.setValidator(validator)
        localLayout.addWidget(minInputField)

        maxInputField = QLineEdit()
        @Slot()
        def handlerMax():
            inputState = maxInputField.text()
            if not inputState:
                argValues[argLabel]["max"] = None
            else:    
                argValues[argLabel]["max"] = float(inputState)
        maxInputField.editingFinished.connect(handlerMax)
        validator = QRegularExpressionValidator("(\-?\d{1,10}(\.\d+)?(E\-?\d{1,2})?)?")
        maxInputField.setValidator(validator)
        localLayout.addWidget(maxInputField)

        row.setLayout(localLayout)
        self.paramsWidget.layout().addWidget(row)

    def clearScrollAreas(self):
        if not self.hyperparamsWidget.layout() is None:
            for i in reversed(range(self.hyperparamsWidget.layout().rowCount())): 
                self.hyperparamsWidget.layout().removeRow(i)
        if not self.paramsWidget.layout() is None:        
            for i in reversed(range(self.paramsWidget.layout().count())): 
                if not self.paramsWidget.layout().itemAt(i).widget().layout() is None:
                    self.paramsWidget.layout().itemAt(i).widget().deleteLater()
            
    def createAndSetInputValidtors(self):
        """Функция установки валидаторов для строк ввода"""
        # Определим валидаторы
        domainValidator = QRegularExpressionValidator("(\d{1,10}(\.\d+)?(E\-?\d{1,2})?)?")
        sizeValidator = QRegularExpressionValidator("\d{0,6}")
        # Установим валидаторы
        self.predStartInput.setValidator(domainValidator)
        self.predStepInput.setValidator(domainValidator)
        self.predSizeInput.setValidator(sizeValidator)
